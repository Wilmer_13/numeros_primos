# Calcular si un número introducido es primo o no.
# author: Wilmer Cruz
# Email: wilmer.cruz@unl.edu.ec

print("Ingrese el valor para determinar si el número es primo o no.")
valor = int(input("Ingreser el número: "))
divisor = 2
while valor>divisor:
    if valor%divisor==0:
        print ("El número ingresado no es primo")
        break
    elif valor%divisor!=0:
        divisor = divisor+1
if valor == divisor:
    print("El número ingresado es primo")


